# Tall Timbers Computer Policy



## Purpose

This repository hosts the most up-to-date computer policy from Tall Timbers. When updates are made, the PDF here will be replaced. A changelog will also be made stating what is updated in the new version of the policy.